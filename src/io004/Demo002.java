package com.hfkh.java54.liupan.io004;

import java.io.UnsupportedEncodingException;

public class Demo002 {
	public static void main(String[] args) throws UnsupportedEncodingException {
		String s = "我爱java";  
		s = new String(s.getBytes("gbk"), "gbk");  
		System.out.println(s);
	}
}
