package com.hfkh.java54.liupan.io004;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Demo011 {
	public static void main(String[] args) throws IOException {
		InputStreamReader isr = new InputStreamReader(new FileInputStream("g://4.java")); 
		char[] ch = new char[1024]; 
		int index = isr.read(ch); 
		System.out.println(new String(ch,0,index)); 
		isr.close();
	}
}
