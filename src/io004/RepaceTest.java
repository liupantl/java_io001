package com.hfkh.java54.liupan.io004;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;


public class RepaceTest {
	public static void main(String[] args) throws IOException {
		//写
		OutputStream os = new FileOutputStream("g://7.txt");
		os.write("您好！我的名字是{name},我是一只{type},我的主人是{master}.".getBytes());
		//读
		Reader in = new FileReader("g://7.txt");
		BufferedReader bf = new BufferedReader(in);
		String str = bf.readLine();
		System.out.println("替换前："+str);
		//替换
		str= str.replace("{name}","欧欧");
		str= str.replace("{type}","拉不拉多");
		str= str.replace("{master}","张三");
		
		//重新存入
		Writer w = new FileWriter("g://8.txt");
		w.write(str);
		
		System.out.println("替换后："+str);
		w.close();
		in.close();
	}
}
