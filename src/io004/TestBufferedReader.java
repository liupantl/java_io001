package com.hfkh.java54.liupan.io004;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class TestBufferedReader {
	public static void main(String[] args) throws IOException {
		Reader str=new FileReader("g://我的青春谁做主.txt ");
		BufferedReader f = new BufferedReader(str);
		System.out.println(f.readLine());
		f.close();
	}
}
