package com.hfkh.java54.liupan.io004;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class TestBufferedWriter {
	public static void main(String[] args) throws IOException {
		FileWriter fw = new FileWriter("g://4.java");
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write("大家好！");
		bw.write("我正在学习BufferedWriter!");
		bw.newLine();//换行
		bw.write("请多多多指教！");
		bw.newLine();
		bw.flush();//刷新缓冲区
		bw.close();
		fw.close();
	}
}
