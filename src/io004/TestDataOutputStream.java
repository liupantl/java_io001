package com.hfkh.java54.liupan.io004;
//二进制文件的读写
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class TestDataOutputStream {
	public static void main(String[] args) throws IOException {
		FileInputStream in = new FileInputStream("g://玻璃球.jpg");
		DataInputStream dis = new DataInputStream(in);
		FileOutputStream outFile = new FileOutputStream("g://玻璃球1.jpg");
		DataOutput out = new DataOutputStream(outFile);
		int temp ;
		while((temp=dis.read())!=-1){
			out.write(temp);
		}
		in.close();
		outFile.close();
	}
}
