package com.hfkh.java54.liupan.io004;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 基本 Java 数据类型 写入写出
 * @author Administrator
 *
 */
public class TestDataOutputStream_1 {
	public static void main(String[] args) throws IOException {
		FileOutputStream fos = new FileOutputStream("c://3.txt");
		DataOutputStream dos = new DataOutputStream(fos);
			dos.write(21);
			dos.writeBoolean(true);
			dos.writeChar('男');
			dos.writeDouble(3.6);
			dos.writeFloat(2.3f);
			dos.writeInt(21);
			dos.writeLong(210000000000l);
			dos.writeUTF("天地玄黄，宇宙洪荒，日月盈仄");
			dos.close();
			
			
		FileInputStream fis = new FileInputStream("c://3.txt");
		DataInputStream dis = new DataInputStream(fis);
		System.out.println(dis.read());
		System.out.println(dis.readBoolean());
		System.out.println(dis.readChar());
		System.out.println(dis.readDouble());
		System.out.println(dis.readFloat());
		System.out.println(dis.readInt());
		System.out.println(dis.readLong());
		System.out.println(dis.readUTF());
		dis.close();
	}
}
