package com.hfkh.java54.liupan.io004;

import java.io.File;
import java.io.IOException;

public class TestFile {
	public static void main(String[] args) throws IOException {
		File file = new File("E://java作业/作业/考试001/答案 - 副本.txt");
		//判断文件夹目录是否存在
		System.out.println(file.exists());
		//判断是否是文件
		System.out.println(file.isFile());
		//判断是否是目录
		System.out.println(file.isDirectory());
		//返回文件相对路径名(输入什么显示什么：java作业\作业\考试001\答案 - 副本.txt)
		System.out.println(file.getPath());
		//返回文件的绝对路径名(全名)
		System.out.println(file.getAbsolutePath());
		//返回文件或目录的名称
		System.out.println(file.getName());
		//删除指定文件夹或目录(成功删除返回true)
		System.out.println(file.delete());
		//创建新的文件(成功创建返回true)
		File newFile = new File("E://java作业/作业/考试001/1.java");
		System.out.println(newFile.createNewFile());
		//返回文件的长度，单位为字节，若文件不存在则返回0L
		System.out.println(newFile.length());
	}
}
