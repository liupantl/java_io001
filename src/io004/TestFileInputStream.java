package com.hfkh.java54.liupan.io004;

import java.io.FileInputStream;
import java.io.IOException;

public class TestFileInputStream {
	public static void main(String[] args) throws IOException {
		FileInputStream file= new FileInputStream("E://java作业/作业/考试001/1.java");
		System.out.println("可读取的字节数："+file.available());
		int num;
		System.out.println("可读取的内容为：");
		while((num=file.read())!=-1){
			System.out.print((char)num+"  ");
		}
		file.close();
				
//		while(file.available()!=0){
//			System.out.print(+(char)file.read());		
//		}	
//		file.close();
	}
}
