package com.hfkh.java54.liupan.io004;

import java.io.FileOutputStream;
import java.io.IOException;

public class TestFileOutputStream {
	public static void main(String[] args) throws IOException {
		FileOutputStream fileOs = new FileOutputStream("G://text.txt");
		String str = "好好学习java！";
		byte[]words =str.getBytes();
		fileOs.write(words,0,words.length);
		System.out.println("hello,文件已更新！");
		fileOs.close();
	}
}
