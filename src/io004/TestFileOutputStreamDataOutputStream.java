package com.hfkh.java54.liupan.io004;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

//与字节流FileOutputStream类实现文本文件读取步骤极其相似。
//按照如下的四个步骤，使用 DataOutputStream写二进制文件
public class TestFileOutputStreamDataOutputStream {
	public static void main(String[] args) throws IOException {
		FileOutputStream fo = new FileOutputStream("g://5.java");
		DataOutputStream od = new DataOutputStream(fo);
		od.writeChars("java doc");
		od.close();
	}
}
