package com.hfkh.java54.liupan.io004;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class TestFileReader {
	public static void main(String[] args) throws IOException {
		Reader file= new FileReader("g://我的青春谁做主.txt");
//		System.out.println((char)file.read());
//		System.out.println((char)file.read());
//		System.out.println((char)file.read());
//		System.out.println((char)file.read());
		
		int num = file.read();
		while(num!=-1){
			System.out.print((char)num+"");
		    num = file.read();
		}
		file.close();
	}
}
