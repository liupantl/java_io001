package com.hfkh.java54.liupan.io004;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class TestInputStreamOutputStream {
	public static void main(String[] args) throws IOException {
		FileInputStream in = new FileInputStream("g://我的青春谁做主.txt");
		FileOutputStream os = new FileOutputStream("g://2.txt");
		byte [] b = new byte[100];
		while(in.available()!=0){
			in.read(b);
			os.write(b);
		}
		System.out.println("复制完成，请查看文件！");
		os.close();
		in.close();
	}
}
