package com.hfkh.java54.liupan.io004;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * 解决中文乱码
 * @author Administrator
 *
 */
public class TestInputStreamReader {
	public static void main(String[] args) throws IOException {
		InputStream in = new FileInputStream("g://6.txt");
		InputStreamReader bf = new InputStreamReader(in,"gbk");
		char [] ch = new char[1024];
		bf.read(ch);
		System.out.println(new String(ch));
	}
}
