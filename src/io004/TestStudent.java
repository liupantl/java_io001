package com.hfkh.java54.liupan.io004;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestStudent {
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		/**
		 * 序列化:对象==》文件
		 */
		List<Student> studentList1 = new ArrayList<Student>();
		studentList1.add(new Student("张三",21,"男"));
		studentList1.add(new Student("李四",22,"男"));
		
		List<Student> studentList2 = new ArrayList<Student>();
		studentList2.add(new Student("张岚",25,"女"));
		studentList2.add(new Student("李奎",24,"男"));
		
		List<Student> studentList3 = new ArrayList<Student>();
		studentList3.add(new Student("张三丰",21,"男"));
		studentList3.add(new Student("李玉霞",22,"女"));
		
		Map<Integer,List<Student>> studentMap = new HashMap<Integer,List<Student>>();
		studentMap.put(1, studentList1);
		studentMap.put(2, studentList2);
		studentMap.put(3, studentList3);
		
		FileOutputStream fos = new FileOutputStream("c://2.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(studentMap);
		oos.close();
		
		/**
		 * 反序列化：文件==》对象
		 */
		FileInputStream fis = new FileInputStream("c://2.txt");
		ObjectInputStream ois = new ObjectInputStream(fis);
		ois.readObject();
		for(Integer key:studentMap.keySet()){
			List<Student> studentList=studentMap.get(key);
			for(Student s : studentList){
				System.out.println(s.getName()+" "+s.getAge()+" "+s.getSex());
			}
		}
	}
}
