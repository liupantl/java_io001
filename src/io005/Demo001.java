package com.hfkh.java54.liupan.io005;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Demo001 {
	public static void main(String[] args) throws IOException {
		//写入0-9
		OutputStream os = new FileOutputStream("G://新建文件夹 (2)/a.txt");
		os.write("0123456789".getBytes());
		//读取
		FileInputStream in = new FileInputStream("G://新建文件夹 (2)/a.txt");
		File file = new File("G://新建文件夹 (2)/a.txt");
		byte [] b = new byte[(int) file.length()];
		in.read(b);
		System.out.println(new String(b));
		//分开重新写入
		for (int i = 0; i < b.length; i++) {
			String str =new String(b).substring(i,i+1);
			OutputStream oss = new FileOutputStream("G://新建文件夹 (2)/"+i+".txt");
			oss.write(str.getBytes());
		}
	}	
}
