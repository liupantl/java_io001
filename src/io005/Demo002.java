package com.hfkh.java54.liupan.io005;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Demo002 {
	public static void main(String[] args) throws IOException {
		//读取
		FileInputStream in = new FileInputStream("G://神雕侠侣/神雕侠侣.txt");
		File file = new File("G://神雕侠侣/神雕侠侣.txt");
		byte [] b = new byte[(int) file.length()];
		in.read(b);
		String sdxlTxt = new String(b);
		System.out.println(sdxlTxt.length());//打印长度

		String [] numStr = {"一","二","三","四","五","六","七","八","九","十"
				,"十一","十二","十三","十四","十五","十六","十七","十八","十九","二十",
				"二十一","二十二","二十三","二十四","二十五","二十六","二十七","二十八","二十九","三十",
				"三十一","三十二","三十三","三十四","三十五","三十六","三十七","三十八","三十九","四十"};

		OutputStream os = null;
		int i =1;
		while(true){
			//何时结束
			if(i==numStr.length){
				break;
			}
			int index1 = sdxlTxt.indexOf("第"+numStr[i-1]+"回");
			int index2 = sdxlTxt.indexOf("第"+numStr[i]+"回");
			os = new FileOutputStream("G://神雕侠侣/"+sdxlTxt.substring(index1, index1+8)+".txt");	
			os.write(sdxlTxt.substring(index1, index2).getBytes());
			i++;
		}
		os=new FileOutputStream("G://神雕侠侣/第四十回华山之巅.txt");
		os.write(sdxlTxt.substring(sdxlTxt.indexOf("第四十回"),sdxlTxt.indexOf("《后 记》")).getBytes());
		
		os=new FileOutputStream("G://神雕侠侣/后 记.txt");
		os.write(sdxlTxt.substring(sdxlTxt.indexOf("《后 记》")).getBytes());
	}
}
